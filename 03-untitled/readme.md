##Технические требования к верстке:

* ~~Верстка должна быть фиксированной ширины~~
* ~~Все лого (и иконка, и текст) должно быть сделано в виде кликабельной ссылки~~  
* ~~Слова Milan Holidays должны быть сделаны в виде текста, не картинкой~~  
* ~~Контент должен занимать ровно ту ширину, какая указана на макете, и находиться по центру страницы~~  
* ~~Полное соответствие всех размеров и отступов тем, что на макете~~  
* ~~Картинки и текст под ними должны быть сделаны в виде кликабельной ссылки~~  
* ~~Стрелочка возле надписи View Details должна быть сделана с помощью псевдоэлемента~~  
* ~~В футере необходимо использовать спецсимвол копирайта~~    
* ~~Логотип и контакты в верху страницы, а также все карточки с фотографиями должны быть позиционированы с помощью CSS свойства float~~  



##Необязательное задание продвинутой сложности:

* ~~Сделать градиентную рамку вокруг картинок с помощью CSS~~    
* ~~Добавить анимацию (появление снизу) для оранжевого блока на картинке при наведении курсора мыши~~  

##Ошибки

* ~~Title страницы (текст в теге Title) учимся сразу правильно оформлять, не просто ДЗ, Document, Title, Homework, а кратко - название страницы. Ее смысл, что на ней находится~~  
* ~~https://prnt.sc/rr2oa0 слишком жирно для маленького квадрата использовать тег секции. используй див~~  
* ~~https://prnt.sc/rr2oo6 ты даже класс называешь навбар контейнер. Почему тогда тег section, а не например nav, или header~~  
* https://prnt.sc/rr2p2t тоже слишком жирно для секции. и такие вещи как контакты обычно оборачиваются в ссылку, а в хрефе есть еще mailto. Подробнее - http://htmlbook.ru/samhtml/ssylki/ssylka-na-adres-elektronnoy-pochty  
* https://prnt.sc/rr2pmr просвечивается  
* ~~https://prnt.sc/rr2pxb когда ты элементу прописываешь float, то он по умолчанию становится display: block, и твой inline-block - бесполезен)~~  
* https://prnt.sc/rr2q77 лучше делать через img, так как изображения - осмысленные  